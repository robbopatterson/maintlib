package maintlib;
import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;

import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

/**
 * VehicleBuilder simplifies creation of a Vehicle object by using a fluent interface.  Example usage:
 * 	Vehicle myClassicCar = 
 * 		VehicleBuilder.INSTANCE.make("Chevrolet").model("Corvette Stingray").year(1969).type(GAS).build();
 */
public class VehicleBuilder {
	
	public static VehicleBuilder INSTANCE = new VehicleBuilder();
	
	private VehicleImpl _nextVehicle;
	private Map<MaintainanceType,Integer> _maintLastPerformedMap;
	
	VehicleBuilder() {
		initializeForNextVehicle();
	}
	
	public Vehicle build() {
		useGasIfNoFuelTypeSpecified();
		VehicleImpl newlyBuiltVehicle = _nextVehicle;
		newlyBuiltVehicle.addMaintainanceTasksForEngineType(newlyBuiltVehicle.getEngineType());
		updateMaintainTaskLastPerformed(newlyBuiltVehicle);
		initializeForNextVehicle();
		return newlyBuiltVehicle;
	}
	
	private void updateMaintainTaskLastPerformed(VehicleImpl newlyBuiltVehicle) {
		for ( MaintainanceTask task : newlyBuiltVehicle.getAllMaintainanceTasks() ) {
			if ( _maintLastPerformedMap.containsKey( task.getType() ) ) {
				task.setLastPerformedAt(_maintLastPerformedMap.get(task.getType()));
			}
		}
	} 
	
	private void useGasIfNoFuelTypeSpecified() {
		if ( _nextVehicle.getEngineType()==null )
			 type(EngineType.gas);
	}
	
	/**
	 * Reset this builder to it's initial state
	 */
	private void initializeForNextVehicle() {
		_nextVehicle = new VehicleImpl( new UID().toString() );
		_maintLastPerformedMap = new HashMap<MaintainanceType,Integer>();
	}
	
	public VehicleBuilder make(String make) {
		_nextVehicle.setMake(make);
		return this;
	}
	public VehicleBuilder model(String model) {
		_nextVehicle.setModel(model);
		return this;
	}
	public VehicleBuilder year(int year) {
		_nextVehicle.setYear(year);
		return this;
	}
	public VehicleBuilder type(EngineType engineType) {
		_nextVehicle.setEngineType(engineType);
		return this;
	}
	public VehicleBuilder odo(int latestOdometerReading) {
		_nextVehicle.setLastOdometerKms(latestOdometerReading);
		return this;
	}
	public VehicleBuilder maintPerformed(MaintainanceType type, int lastDoneAtKms) {
		_maintLastPerformedMap.put(type, lastDoneAtKms);
		return this;
	}
}
