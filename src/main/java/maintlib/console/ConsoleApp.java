package maintlib.console;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import maintlib.MaintainanceTask;
import maintlib.Vehicle;
import maintlib.VehicleBuilder;
import maintlib.Vehicles;
import maintlib.pojo.EngineType;

public class ConsoleApp {

	private Vehicles vehicles = new Vehicles();
	
	public static void main(String[] args) {
		ConsoleApp app = new ConsoleApp();
		app.start();
	}
	
	public void start() {
		System.out.println("Welcome To the Automobile Maintainance Tracker testing console.\r\n");
		System.out.println("A programming problem for CurveDental written by Robert Patterson.\r\n");
		System.out.println("\r\nWould you like to start with an Empty dataset, an Example dataset?");
		char response = getUserResponse("Please Enter E of Empty or X for eXample","EX", 'X');
		if (response=='X')
			ExampleDataLoader.load(vehicles);
		
		doList();
		
		do {
			response = getUserResponse("\nEnter Command: R->Remove, A->Add, U->Update, L->List, X->eXit","RAULX", ' ');
			switch(response) {
			case 'R': doRemove(); break;
			case 'A': doAdd(); break;
			case 'U': doUpdate(); break;
			case 'L': doList(); break;
			}
		} while( response!='X' );
	}
	
	private void doRemove(){
		Vehicle v= selectVehicle();
		char response = getUserResponse( String.format("\n%s\nConfirm Removal?",v.getDescription()), "YN", 'N');
		if ( response=='Y' ) {
			vehicles.removeVehicleById(v.getId());
		    System.out.println("Removed.");
		} else  {
			System.out.println("Nothing removed.");
		}
		
	}
	private void doUpdate(){
		Vehicle v = selectVehicle();
		if ( v!=null ) {
			System.out.println(v.getDescription());
			int odo = getUserResponseInt("Current Odometer reading (in kms): ");
			if (odo==0) {
				System.out.println("No update done.");
			}
			v.setLastOdometerKms(odo);
			System.out.println(v.getDescription());
			for( MaintainanceTask task : v.findMaintainanceTasksCurrentlyDue() ) {
				char response = getUserResponse("  was "+task.getType().toString()+" completed at this time?", "YN", 'N');
				if ( response=='Y' )
					task.setLastPerformedAt(odo);
			}
		}
	}

	private void doAdd(){

		Vehicle v = VehicleBuilder.INSTANCE
			.make( getUserResponseStr("make") )
			.model( getUserResponseStr("model") )
			.year( getUserResponseInt("year") )
			.odo( getUserResponseInt("odometer(kms)") )
			.type( getEngineType() )
			.build();
		
		char response = getUserResponse( String.format("\n%s\nIs this correct?",v.getDescription()), "YN", 'Y');
		if ( response=='Y' ) {
			vehicles.addVehicle(v);
		    System.out.println("Added.");
		} else  {
			System.out.println("Nothing added.");
		}
	}
	
	private EngineType getEngineType() {
		char response = getUserResponse( "engine type G=Gas, D=Diesel, H=Hybrid, E=Electric", "GDEH", 'G' );
		switch(response) {
		case 'D': return EngineType.diesel;
		case 'H': return EngineType.hybrid;
		case 'E': return EngineType.electric;
		default: 
		}
		return EngineType.gas;
	}

	private String getUserResponseStr( String prompt ) {
		System.out.println( String.format("%s: ", prompt) );
		return System.console().readLine();
	}

	private int getUserResponseInt( String prompt ) {
		System.out.println( String.format("%s: ", prompt) );
		String s = System.console().readLine();
		try {
			return Integer.parseInt(s);
		} catch ( NumberFormatException e ) {
			return getUserResponseInt( prompt );
		}
	}
	
	private char getUserResponse( String prompt, String availableOptions, char defaultIfReturn ) {
		boolean isResponseValid;
		char responseChar;
		do {
			if (defaultIfReturn==' ')
				System.out.println( String.format("%s: ", prompt) );
			else
			    System.out.println( String.format("%s (default='%c'): ", prompt, defaultIfReturn) );
			String response = System.console().readLine().trim().toUpperCase();
			responseChar = defaultIfReturn;
			if (response.length()>0)
				responseChar = response.charAt(0);
			isResponseValid = availableOptions.indexOf(responseChar) >= 0;
			if ( !isResponseValid )
				System.out.println( "Invalid!" );
		} while( !isResponseValid );

		return responseChar;
	}
	
	private SortedMap<Integer,Vehicle> doList() {
		System.out.println( "Vehicle list:");
		SortedMap<Integer,Vehicle> map = vehicleSelectionMap(vehicles.getAllVehicles());
		for( Entry<Integer,Vehicle> entry : map.entrySet()  ) {
			System.out.println( entry.getKey() + " -> " + entry.getValue().getDescription() );
		}
		return map;
	} 

	private Vehicle selectVehicle() {
		SortedMap<Integer,Vehicle> map = doList();
		int i = getUserResponseInt("Select Vehicle (or 0 to abort): ");
		if ( i <1 || i > map.size() )
			return null;
		return map.get(i);
	}
	
	private static SortedMap<Integer,Vehicle> vehicleSelectionMap(Collection<Vehicle> vehicles) {
		
		SortedMap<Integer,Vehicle> map = new TreeMap<Integer, Vehicle>();
		int i=1;
		for( Vehicle vehicle : vehicles  ) {
			map.put(i++, vehicle);
		}
		return map;
	}

}
