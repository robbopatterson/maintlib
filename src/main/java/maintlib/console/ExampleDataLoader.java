package maintlib.console;

import maintlib.VehicleBuilder;
import maintlib.Vehicles;
import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

public class ExampleDataLoader {

	public static void load(Vehicles vehicles) {
		VehicleBuilder b = VehicleBuilder.INSTANCE;
		vehicles.addVehicle( b.make("Ford").model("Focus").year(2011).odo(123456)
				.maintPerformed(MaintainanceType.oil_change, 100000)
				.build() );
		vehicles.addVehicle( 
				b.make("Ford").model("F150").year(1998).odo(320000).type(EngineType.diesel)
				.maintPerformed(MaintainanceType.oil_change, 300000)
				.maintPerformed(MaintainanceType.drain_water_separators, 250000)
				.maintPerformed(MaintainanceType.change_airfilter, 280000)
				.build() );
		vehicles.addVehicle( b.make("Chevrolet").model("Corvette Stingray").year(1969).odo(223000).build() );
		vehicles.addVehicle( b.make("Toyota").model("Prius").year(2008).type(EngineType.hybrid).odo(168000)
				.maintPerformed(MaintainanceType.tire_rotation, 100000)
				.build() );
		vehicles.addVehicle( b.make("Telsa").model("Model-S").year(2013).odo(75000).type(EngineType.electric).build() );
		vehicles.addVehicle( b.make("Nissan").model("Leaf").year(2014).odo(38000).type(EngineType.electric).build() );
	}
}
