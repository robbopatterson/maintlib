package maintlib;

import java.util.Collection;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import maintlib.pojo.MaintainanceType;

public class MaintainanceTasksFilter {

	public static Collection<MaintainanceTask> matchesType( final MaintainanceType type, Collection<MaintainanceTask> source ) {
		return Collections2.filter(source, new Predicate<MaintainanceTask>() {
			public boolean apply(MaintainanceTask task) { return task.getType()==type; }
		});
	}

	public static Collection<MaintainanceTask> maintainanceDue( final Vehicle v, Collection<MaintainanceTask> source ) {
		return Collections2.filter(source, new Predicate<MaintainanceTask>() {
			public boolean apply(MaintainanceTask task) { return task.isMaintainanceDue(v); }
		});
	}
}
