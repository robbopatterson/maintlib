package maintlib;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;
import maintlib.pojo.VehicleInfo;

public class VehicleImpl extends VehicleInfo implements Vehicle {

	private Set<MaintainanceTask> _tasks = new HashSet<MaintainanceTask>();
	
	/**
	 * Protected constructor.  Use VehicleBuilder to construct
	 */
	protected VehicleImpl(String id) {
		setId(id);
	}

	public Collection<MaintainanceTask> getAllMaintainanceTasks() {
		return _tasks;
	}

	public MaintainanceTask findMaintainanceTask(final MaintainanceType findType) {
		for( MaintainanceTask task : _tasks ) {
			if ( task.getType()==findType )
				return task;
		}
		return null; // Engine type does not require this maintainance
	}

	public Collection<MaintainanceTask> findMaintainanceTasksCurrentlyDue() {
		return MaintainanceTasksFilter.maintainanceDue( this, _tasks );
	}
	

	protected void addMaintainanceTasksForEngineType(EngineType engineType) {
		MaintainanceType[] maintainanceTypes = maintainanceTasksForEngineType.get(engineType);
		for( MaintainanceType maintType : maintainanceTypes ) {
			MaintainanceTask task = new MaintainanceTask(maintType);
			task.setLastPerformedAt(getLastOdometerKms());
			_tasks.add(task);
		}
	}
	
	private static Map<EngineType,MaintainanceType[]> maintainanceTasksForEngineType = new HashMap<EngineType, MaintainanceType[] >();
	static {
		maintainanceTasksForEngineType.put(EngineType.gas, new MaintainanceType[] {
				MaintainanceType.oil_change, MaintainanceType.tire_rotation, MaintainanceType.change_airfilter, MaintainanceType.change_fuelfilter}
		);
		maintainanceTasksForEngineType.put(EngineType.diesel, new MaintainanceType[] {
				MaintainanceType.oil_change, MaintainanceType.tire_rotation, MaintainanceType.change_airfilter, MaintainanceType.change_fuelfilter, MaintainanceType.drain_water_separators}
		);
		maintainanceTasksForEngineType.put(EngineType.hybrid, new MaintainanceType[] {
				MaintainanceType.oil_change, MaintainanceType.tire_rotation, MaintainanceType.change_airfilter, MaintainanceType.change_fuelfilter}
		);
		maintainanceTasksForEngineType.put(EngineType.electric, new MaintainanceType[] {
				MaintainanceType.tire_rotation }
		);
	}

	public String getDescription() {
		StringBuffer sb = new StringBuffer();
		if ( getYear()!=0 ) {
			sb.append(" "+getYear());
		}
		if ( getMake()!=null && getMake().length()>0 ) {
			sb.append(" "+getMake());
		}
		if ( getModel()!=null && getModel().length()>0 ) {
			sb.append(" "+getModel());
		}
		if ( getEngineType()!=EngineType.gas ) {
			sb.append("[");
			sb.append(getEngineType().toString());
			sb.append("]");
		}
		if ( getLastOdometerKms()!=0 ) {
			sb.append(" odometer=");
			sb.append(getLastOdometerKms());
			sb.append("kms");
		}
		
		for( MaintainanceTask task : findMaintainanceTasksCurrentlyDue() ) {
			sb.append("\n          due for: "+task.getType().toString()+" (last performed at "+task.getLastPerformedAt()+ "kms)");
		}
		
		return sb.toString();
	}

}
