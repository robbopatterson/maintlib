package maintlib;

import maintlib.pojo.MaintainanceTaskInfo;
import maintlib.pojo.MaintainanceType;

public class MaintainanceTask extends MaintainanceTaskInfo {
	
	MaintainanceTask( MaintainanceType maintainanceType ) {
		setType(maintainanceType);
	}
	
	public boolean isMaintainanceDue( Vehicle v ) {
		int odometerKms = v.getLastOdometerKms();
		int interval = RecommendedMaintainanceSchedule.kmsBetweenService(v,this.getType());
		return odometerKms >= getLastPerformedAt()+interval;
	}
		
}
