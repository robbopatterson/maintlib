package maintlib.pojo;

public enum MaintainanceType {
	oil_change,
	tire_rotation,
	change_airfilter,
	change_fuelfilter,
	drain_water_separators,
}
