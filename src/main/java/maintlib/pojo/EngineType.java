package maintlib.pojo;

public enum EngineType {
	gas,
	diesel,
	electric,
	hybrid,
}
