package maintlib.pojo;

public class MaintainanceTaskInfo {
	
	private MaintainanceType _type;
	private int _lastOdometerKm;
	
	public MaintainanceType getType() {
		return _type;
	};
	public void setType( MaintainanceType type ) {
		_type = type;
	};
	public void setLastPerformedAt( int lastOdometerKm ) {
		_lastOdometerKm = lastOdometerKm;
	} 
	public int getLastPerformedAt() {
		return _lastOdometerKm;
	} 
	

}
