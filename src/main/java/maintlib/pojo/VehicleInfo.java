package maintlib.pojo;


public class VehicleInfo {

	private String _id;
	
	private EngineType _engineType;
	private String _make;
	private String _model;
	private int _year;
	private int _lastOdometerKms;
	
	public String getId() {
		return _id;
	}
	public String getMake() {
		return _make;
	}
	public String getModel() {
		return _model;
	}
	public int getYear() {
		return _year;
	}
	public int getLastOdometerKms() {
		return _lastOdometerKms;
	}
	public EngineType getEngineType() {
		return _engineType;
	}

	public void setId(String id) {
		_id = id;
	}
	public void setMake(String make) {
		_make = make;
	}
	public void setModel(String model) {
		_model = model;
	}
	public void setYear(int year) {
		_year = year;
	}
	public void setLastOdometerKms( int lastOdometerKms ) {
		_lastOdometerKms = lastOdometerKms;
	}
	public void setEngineType( EngineType engineType ) {
		_engineType = engineType;
	}
}
