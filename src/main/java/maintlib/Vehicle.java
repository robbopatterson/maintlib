package maintlib;

import java.util.Collection;

import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

public interface Vehicle {
	
	String getDescription();
	
	String getId(); // A unique identifier for this vehicle

	EngineType getEngineType();
	String getMake();
	String getModel();
	int getYear();
	int getLastOdometerKms();

	void setLastOdometerKms( int odometerKms );
	
	MaintainanceTask findMaintainanceTask( MaintainanceType type ); // Returns null if unsupported

	Collection<MaintainanceTask> findMaintainanceTasksCurrentlyDue();
}
