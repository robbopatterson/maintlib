package maintlib;

import java.util.HashMap;
import java.util.Map;

import maintlib.pojo.MaintainanceType;

public class RecommendedMaintainanceSchedule {
	
	public static int kmsBetweenService( Vehicle v, MaintainanceType type ) {
		return kmBetweenService.get(type);
	}
	
	private static Map<MaintainanceType, Integer> kmBetweenService = new HashMap<MaintainanceType, Integer >();
	static {
		kmBetweenService.put(MaintainanceType.change_airfilter, 10000);
		kmBetweenService.put(MaintainanceType.change_fuelfilter,20000);
		kmBetweenService.put(MaintainanceType.drain_water_separators, 50000);
		kmBetweenService.put(MaintainanceType.oil_change, 8000);
		kmBetweenService.put(MaintainanceType.tire_rotation, 20000);
	}
	

	
}
