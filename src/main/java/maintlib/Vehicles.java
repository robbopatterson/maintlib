package maintlib;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Vehicles {
	private Map<String,Vehicle> _vehicleMap = new HashMap<String,Vehicle>();
	
	public Collection<Vehicle> getAllVehicles() {
		return _vehicleMap.values();
	}
	
	public void addVehicle(Vehicle vehicle) {
		_vehicleMap.put(vehicle.getId(),vehicle);
	}

	public void removeVehicleById(String id) {
		_vehicleMap.remove(id);
	}

}
