package maintlib;

import static org.junit.Assert.*;

import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

import org.junit.Test;

public class RecommendedMaintainanceScheduleTest {

	@Test
	public void kmsBetweenService_reportsDifferentServiceInterval_forDifferenceMaintainanceTypes() {
		Vehicle v = VehicleBuilder.INSTANCE.type(EngineType.gas).build();
		int kmsBetweenOilChange = RecommendedMaintainanceSchedule.kmsBetweenService(v, MaintainanceType.oil_change);
		int kmsBetweenTireRotation = RecommendedMaintainanceSchedule.kmsBetweenService(v, MaintainanceType.tire_rotation);
		assertNotEquals(kmsBetweenOilChange, kmsBetweenTireRotation);
	}

}
