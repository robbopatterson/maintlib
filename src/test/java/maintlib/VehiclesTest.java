package maintlib;

import static org.junit.Assert.*;

import org.junit.Test;

public class VehiclesTest {

	@Test
	public void getAllVehicles_initially_shouldReturnEmptyCollection() {
		Vehicles records = new Vehicles();
		assertEquals(0, records.getAllVehicles().size() );
	}
	
	@Test
	public void addVehicle_multipleAdditions_updatesVehicelsCount() {
		Vehicles records = new Vehicles();
		records.addVehicle( VehicleBuilder.INSTANCE.build() );
		records.addVehicle( VehicleBuilder.INSTANCE.build() );
		assertEquals( 2, records.getAllVehicles().size() );
	}
}
