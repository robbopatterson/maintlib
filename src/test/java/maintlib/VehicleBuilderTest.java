package maintlib;

import static org.junit.Assert.*;
import maintlib.Vehicle;
import maintlib.VehicleBuilder;
import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

import org.junit.Test;

public class VehicleBuilderTest {

	@Test
	public void build_newInstance_defaultEngineTypeIsGas() {
		Vehicle v1 = VehicleBuilder.INSTANCE.build();
		assertEquals(EngineType.gas, v1.getEngineType());
	}

	@Test
	public void build_calledTwice_returnsUniqueVehicleIds() {
		Vehicle v1 = VehicleBuilder.INSTANCE.build();
		Vehicle v2 = VehicleBuilder.INSTANCE.build();
		assertNotEquals(v1.getId(), v2.getId());
	}

	@Test
	public void build_settingMultipleFields_returnsExpectedVehicle() {
		Vehicle myEcoCar = VehicleBuilder.INSTANCE
							.make("Nissan")
							.model("Leaf")
							.type(EngineType.electric)
							.build();
		assertEquals("Nissan", myEcoCar.getMake());
		assertEquals("Leaf", myEcoCar.getModel());
		assertEquals(EngineType.electric, myEcoCar.getEngineType());
	}

	@Test
	public void build_maintainanceTasks_areUpdatedAsExpected() {
		Vehicle v = VehicleBuilder.INSTANCE
							.make("Ford")
							.model("F-150")
							.type(EngineType.diesel)
							.odo(100000)
							.maintPerformed(MaintainanceType.change_airfilter, 60000)
							.maintPerformed(MaintainanceType.drain_water_separators, 0)
							.maintPerformed(MaintainanceType.oil_change, 100000)
							.build();
		assertEquals("Expect airfilter and water seperators are due", 2, v.findMaintainanceTasksCurrentlyDue().size());
	}


}
