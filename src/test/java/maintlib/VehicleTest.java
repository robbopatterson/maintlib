package maintlib;

import static org.junit.Assert.*;
import maintlib.pojo.EngineType;
import maintlib.pojo.MaintainanceType;

import org.junit.Test;

public class VehicleTest {

	@Test
	public void getMaintainanceTasks_byEngineType_includesOilChangeOnlyWhenAppropriate() {
		Vehicle gasVehicle		= VehicleBuilder.INSTANCE.type(EngineType.gas).build();
		Vehicle dieselVehicle	= VehicleBuilder.INSTANCE.type(EngineType.diesel).build();
		Vehicle electricVehicle	= VehicleBuilder.INSTANCE.type(EngineType.electric).build();
		Vehicle hybridVehicle	= VehicleBuilder.INSTANCE.type(EngineType.hybrid).build();

		assertNotNull( gasVehicle.findMaintainanceTask(MaintainanceType.oil_change) );
		assertNotNull( dieselVehicle.findMaintainanceTask(MaintainanceType.oil_change) );
		assertNotNull( hybridVehicle.findMaintainanceTask(MaintainanceType.oil_change) );
		assertNull( electricVehicle.findMaintainanceTask(MaintainanceType.oil_change) );
	}

	@Test
	public void findMaintainanceTasksCurrentlyDue_whenOdoUpdated_oilChangeDueAfter8000KM() {
		
		Vehicle v = VehicleBuilder.INSTANCE.type(EngineType.gas).build();
		v.findMaintainanceTask(MaintainanceType.oil_change).setLastPerformedAt(100000);
		
		v.setLastOdometerKms(107999);
		assertEquals(0,MaintainanceTasksFilter.matchesType(MaintainanceType.oil_change,v.findMaintainanceTasksCurrentlyDue() ).size() );

		v.setLastOdometerKms(108000);
		assertEquals(1,MaintainanceTasksFilter.matchesType(MaintainanceType.oil_change,v.findMaintainanceTasksCurrentlyDue() ).size() );
		v.setLastOdometerKms(200000);
		assertEquals(1,MaintainanceTasksFilter.matchesType(MaintainanceType.oil_change,v.findMaintainanceTasksCurrentlyDue() ).size() );

	}

}
